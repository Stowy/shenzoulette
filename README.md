# Shenzoulette

# Getting started

## The config file
For the bot to work, it needs a config.json file.

Create a config.json at the root of the project and put this in it:

```Json
{
    "token": "BOT_TOKEN",
    "prefix": "!"
}
```