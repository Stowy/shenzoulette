﻿using DSharpPlus;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.EventArgs;
using DSharpPlus.Interactivity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics.CodeAnalysis;

namespace Shenzoulette.Commands
{
    [SuppressMessage("Performance", "CA1822:Mark members as static")]
    public class FunCommands : BaseCommandModule
    {
        [Command("ping")]
        [Description("Sends pong.")]
        public async Task Ping(CommandContext ctx)
        {
            await ctx.Channel.SendMessageAsync("Pong")/*.ConfigureAwait(false)*/;
        }

        [Command("add")]
        [Description("Adds two numbers together")]
        [RequirePermissions(Permissions.Administrator)]
        public async Task Add(
            CommandContext ctx, 
            [Description("First number")] int nbrOne, 
            [Description("Second number")] int nbrTwo)
        {
            await ctx.Channel.SendMessageAsync((nbrOne + nbrTwo).ToString());
        }

        [Command("respondReaction")]
        public async Task RespondEmoji(CommandContext ctx,[Description("Time to react in seconds. Defaults to 120.")] int timeToReact = 120)
        {
            InteractivityExtension interactivity = ctx.Client.GetInteractivity();
            InteractivityResult<MessageReactionAddEventArgs> message = 
                await interactivity.WaitForReactionAsync(x => x.Channel == ctx.Channel, TimeSpan.FromSeconds(timeToReact));
            await ctx.Channel.SendMessageAsync(message.Result.Emoji);
        }
    }
}
