﻿using Newtonsoft.Json;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Shenzoulette
{
    public struct ConfigJson
    {
        [JsonProperty("token")]
        public string Token { get; private set; }

        [JsonProperty("prefix")]
        public string Prefix { get; private set; }

        /// <summary>
        /// Creates a new ConfigJson from a file.
        /// </summary>
        /// <param name="path">Path to the config.json file.</param>
        /// <returns>Returns a new ConfigJson with the data from config.json.</returns>
        public static async Task<ConfigJson> LoadFromFileAsync(string path)
        {
            string json = string.Empty;
            using (FileStream fs = File.OpenRead(path))
            using (StreamReader sr = new StreamReader(fs, new UTF8Encoding(false)))
                json = await sr.ReadToEndAsync();

            // Load the config file in a struct
            return JsonConvert.DeserializeObject<ConfigJson>(json);
        }

        public override bool Equals(object obj)
        {
            bool isEqual = false;
            if (obj is ConfigJson config)
                isEqual = Token == config.Token && Prefix == config.Prefix;

            return isEqual;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public static bool operator ==(ConfigJson left, ConfigJson right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(ConfigJson left, ConfigJson right)
        {
            return !(left == right);
        }
    }
}
